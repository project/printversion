
var Printversion = {  
  added: [],
  destination: "",
  has_pvs: function() {
    return (location.href).match(/\/printable_version$/i);
  },
  createStyle: function(item) {
    var index, path;
    var item_path = item.innerHTML;
    for (i in document.styleSheets) {
      var style = document.styleSheets[i];
      if (style.imports && (style.imports.length>0) ) {
        path = style.imports[0].href;
        if (item_path.indexOf(path) != -1) {
          index = i;break;
        }
      }
    }
    
    if (index) {
      var sheet = document.createStyleSheet('', index); //path
      sheet.media = "screen";
      sheet.addImport(path);
      var elem = sheet.owningElement;
      elem.type = item.type;
      Printversion.added.push([elem]);
    }  
  },
  addPrintversionButton: function() {
    var div=$('<div id="pvs-buttons-wrapper"><div id="pvs-buttons"><button id="pvs-print" /><button id="pvs-close" /></div></div>').prependTo('body');
    $('#pvs-print', div).click(function() {window.print();});
    $('#pvs-close', div).click(function() {Printversion.switchToNormalLayout();});
  },
  removePrintversionButton: function(){
    $('#pvs-buttons').remove();
  },
  switchToPrintLayout: function(){    
    $("style,link[@rel='stylesheet']").filter("[@media='print']").each(
      function() {
        var arg = this.outerHTML;
        if (arg) {
          var div = document.createElement("div");
          div.innerHTML = "div<div>" + arg + "</div>";
          var elem = div.lastChild.childNodes[0];
          try {
            elem.media = "screen";  
          } catch(e) {
            elem = createStyle(this); // not good IE 6.0.3790
          }
        }
        else {
          var elem = this.cloneNode(true);
          elem.media = "screen";
        }
        if (elem) {
          this.parentNode.insertBefore(elem, this);
          Printversion.added.push([elem]);
        }
      }
    );
    this.addPrintversionButton();
  },
  switchToNormalLayout: function(){
    this.removePrintversionButton();
    for (i in this.added) {$(this.added[i]).remove();}
    this.added = [];
    if ($.browser.msie) {
      $("style,link[@rel='stylesheet']").not("[@media='print']").each(
        function() {this.disabled = true; this.disabled = false;}
      );
    }
    if (this.has_pvs()) {
      var path = location.href;
      if (this.destination.length > 0) window.location.href = "/" + this.destination;
      else window.location.href = "/" + path.replace(/\/printable_version$/i, '');
    }
  }
};

$(function() { 
  $("a[id='printversion']").attr("href", "javascript:Printversion.switchToPrintLayout();");
  if (Printversion.has_pvs()) Printversion.switchToPrintLayout();
});
