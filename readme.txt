
Install
-------------------------------------------------------------------------------
1. Enable the printversion module:
    Administer > Site building > Modules (admin/build/modules)
2. Configure access rules:
    Administer > User management > Access control (admin/user/access)
3. Congigure printversion block:
    Administer > Site building > Blocks (admin/build/block)

Display or hide other page areas
-------------------------------------------------------------------------------
Simply edit print.css. This stylesheet is applying when displaying preview or
when printing (both in preview and full view modes)

